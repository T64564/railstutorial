# Rails チュートリアル
[Ruby on Rails チュートリアル](http://railstutorial.jp/)の全ての章を読み，そこで紹介されているWebアプリケーションを作成しました．  
章で取り扱っている内容ごとにブランチを分けています(第一章，第二章は誤ってmasterブランチで作業を行ったため，chapter1, chapter2のブランチはありません)．
